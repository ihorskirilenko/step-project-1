'use strict'

let grid = document.querySelector('.grid');
let masonry = new Masonry( grid, {
    // options
    itemSelector: '.grid-item',
    columns: 18,
    columnWidth: 63,
    gutter: 10,
});

function getItemElement(arr) {

    let elem = document.createElement('div');
    let wRand = Math.random();
    let randIndex = Math.floor(Math.random() * arr.length);
    let widthClass = wRand > 0.65 ? 'grid-item--width4' : wRand > 0.35 ? 'grid-item--width3' : 'grid-item--width2';
    let heightClass = widthClass === 'grid-item--width4' ? 'grid-item--height4' : widthClass === 'grid-item--width3' ? 'grid-item--height3' : 'grid-item--height2';

    elem.className = `grid-item ${widthClass} ${heightClass}`;
    elem.innerHTML = `<img src="img/gallery/${arr[randIndex]}.png">`;
    arr.splice(randIndex, 1);

    return elem;
}

function insertElements() {

    let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,]

    for (let i = 0; i < 15; i++) {
        let element = getItemElement(arr);
        grid.appendChild(element)
        masonry.appended(element);
        masonry.layout();
    }
}

insertElements();

let btn = document.querySelector('#skills .btn');
btn.addEventListener('click', () => {
    let duration = 2000;
    loaderEmulator(btn, duration);
    setTimeout(() => {
        insertElements();
        grid.childElementCount > 31 ? btn.style.display = 'none' : false;
    }, duration);
})



