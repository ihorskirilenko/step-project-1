'use strict'

function loaderEmulator(caller, dur) {

    let loader = document.createElement('div');
    loader.classList.add('loader')
    loader.innerHTML = `
                          <div class="bar bar1"></div>
                          <div class="bar bar2"></div>
                          <div class="bar bar3"></div>
                          <div class="bar bar4"></div>
                          <div class="bar bar5"></div>
                          <div class="bar bar6"></div>
                          <div class="bar bar7"></div>
                          <div class="bar bar8"></div>
                        `
    caller.insertAdjacentElement('beforebegin', loader);
    setTimeout(() => {
        document.querySelector('.loader').remove();
    }, dur);

}