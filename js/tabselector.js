'use strict'
window.addEventListener('load', () => {
    document.querySelector('.service-tabs-title').classList.add('active');
    document.querySelector('.service-tabs-content div').classList.add('active');

    function tabSelection() {
        let count = 0;
        let navBar = document.querySelectorAll('.service-tabs-title');
        let tabs = document.querySelectorAll('.service-tabs-content > div');
        let clRemove = function(el) {el.classList.remove('active')};

        tabs.forEach(el => {
            el.dataset.nmb = `${count}`;
            count++;
        })

        count = 0;
        navBar.forEach(el => {
            el.dataset.nmb = `${count}`;
            el.addEventListener('click', function() {
                navBar.forEach(el => clRemove(el));
                tabs.forEach(el => clRemove(el));
                this.classList.add('active');
                document.querySelector(`.service-tabs-content div[data-nmb='${this.dataset.nmb}']`).classList.add('active');
            })
            count++;
        })
    }

    tabSelection();
});