'use strict'

window.addEventListener('load', () => {

    //Main ToDos:
    //First page randomizer - done
    //Code refactoring (join listener functions)

    const sliderContainer = document.querySelector('.testimonial-slider-container');
    const size = Object.keys(slider).length;
    const navBar = document.querySelector('.testimonial-slider-nav').children;

    //Core functions

    function changeIconsBG(curr, dir) {
        //expected input: current object ID, direction

        let startColl = document.querySelectorAll('.testimonial-slider-nav img');
        let nextId = getNextObjID(curr, dir);

        let leftColl = [];
        let rightColl = [];


        startColl.forEach((el, index) => {
            index < 3 ? leftColl[index] = el.dataset.id : false;
        })
        startColl.forEach((el, index) => {
            index > 0 ? rightColl[index] = el.dataset.id : false;
        })

        rightColl = rightColl.filter(Boolean);

        if (dir === 'l') {
            startColl.forEach((el, index) => {

                if (index === 0) {
                    el.setAttribute('src', slider[nextId].imgLink);
                    el.dataset.id = nextId;
                } else if (index > 0) {
                    el.setAttribute('src', slider[+leftColl[index - 1]].imgLink);
                    el.dataset.id = `${slider[+leftColl[index - 1]].id}`;
                }
            })
        } else if (dir === 'r') {
            startColl.forEach((el, index) => {

                if (index < 3) {
                    el.setAttribute('src', slider[+rightColl[index]].imgLink);
                    el.dataset.id = `${slider[+rightColl[index]].id}`;
                } else if (index === 3) {
                    el.setAttribute('src', slider[nextId].imgLink);
                    el.dataset.id = nextId;
                }
            })
        }

    }


    function changeMainBG(target, dir) {

        let leftSlide = document.querySelector('.testimonial-slide-wrapper:nth-child(1)').children;
        let centerSlide = document.querySelector('.testimonial-slide-wrapper:nth-child(2)').children;
        let rightSlide = document.querySelector('.testimonial-slide-wrapper:nth-child(3)').children;

        if (dir === 'l') {

            leftSlide[0].innerText = slider[target].text1;
            leftSlide[1].innerText = slider[target].header;
            leftSlide[2].innerText = slider[target].text2;
            leftSlide[3].setAttribute('src', slider[target].imgLink);

            sliderContainer.classList.add('move-left');

        } else if (dir === 'r') {
            rightSlide[0].innerText = slider[target].text1;
            rightSlide[1].innerText = slider[target].header;
            rightSlide[2].innerText = slider[target].text2;
            rightSlide[3].setAttribute('src', slider[target].imgLink);

            sliderContainer.classList.add('move-right');
        }

        setTimeout(() => {
            centerSlide[0].innerText = slider[target].text1;
            centerSlide[1].innerText = slider[target].header;
            centerSlide[2].innerText = slider[target].text2;
            centerSlide[3].setAttribute('src', slider[target].imgLink);

            sliderContainer.classList.remove('move-left', 'move-right');
        }, 1000);

    }


    function getNextObjID(curr, dir) {
        //expected input: current object ID, direction
        //expected output: next (left or right) object ID

        let currSlideId = curr;
        let nextSlideId;

        if (dir === 'r') {
            (currSlideId + 1) > size ? nextSlideId = 1 : nextSlideId = currSlideId + 1;
        } else if (dir === 'l') {
            (currSlideId - 1) < 1 ? nextSlideId = size : nextSlideId = currSlideId - 1;
        }

        return nextSlideId;
    }


    function addActiveClass(dir) {
        Array.from(navBar).forEach(el => {dir ? el.classList.remove('active') : false});
        dir === 'l' ? navBar[2].classList.add('active') : false;
        dir === 'r' ? navBar[3].classList.add('active') : false;
    }


    function validate(curr, target, dir) {
        let c = !!Object.keys(slider).includes(`${target}`);
        let t = !!Object.keys(slider).includes(`${target}`);
        let d = !!(dir === 'l' || dir === 'r');

        return c && t && d;
    }

    //Listeners functions

    function leftArrowListener(ev, dirArg) {

        //Left Arrow click

        let firstIcon = +document.querySelector('.testimonial-slider-nav img:nth-child(2)').dataset.id; //1st img of collection
        let target = +document.querySelector('.testimonial-slider-nav img.active').previousElementSibling.dataset.id;

        let dir;
        dirArg ? dir = dirArg : dir = ev.target.dataset.direction;

        if (firstIcon === target && validate(firstIcon, target, dir)) {
            changeIconsBG(firstIcon, dir);
            addActiveClass(dir);
            changeMainBG(target, dir);
        } else if (validate(firstIcon, target, dir)) {
            changeMainBG(target, dir);
            addActiveClass(dir);
        }

    }

    function centerListener(ev) {

        let dir = ev.target.dataset.direction;
        let target = ev.target.dataset.id;
        if (validate(true, target, dir)) {
            changeMainBG(target, dir);
            addActiveClass(dir);
        }

    }

    function rightArrowListener(ev, dirArg) {

        //Right Arrow click
        let lastIcon = +document.querySelector('.testimonial-slider-nav img:nth-child(5)').dataset.id;
        let target = +document.querySelector('.testimonial-slider-nav img.active').nextElementSibling.dataset.id;

        let dir;
        dirArg ? dir = dirArg : dir = ev.target.dataset.direction;

        if (lastIcon === target && validate(lastIcon, target, dir)) {
            changeIconsBG(lastIcon, dir);
            changeMainBG(target, dir);
        } else if (validate(lastIcon, target, dir)) {
            changeMainBG(target, dir);
            addActiveClass(dir);
        }

    }


    window.addEventListener('keydown', (ev) => {
        ev.key === 'ArrowLeft' ? leftArrowListener(ev, 'l') : false;
        ev.key === 'ArrowRight' ?  rightArrowListener(ev, 'r') : false;
    })


    navBar[0].addEventListener('click', ev => leftArrowListener(ev));
    navBar[1].addEventListener('click', ev => {

        //left Image click
        let firstIcon = +ev.target.dataset.id;

        let dir = ev.target.dataset.direction;
        let target = +ev.target.dataset.id;

        if (validate(firstIcon, target, dir)) {
            changeIconsBG(firstIcon, dir);
            changeMainBG(target, dir);
            addActiveClass(dir);
        }
    });

    navBar[2].addEventListener('click', ev => centerListener(ev));
    navBar[3].addEventListener('click', ev => centerListener(ev));
    navBar[4].addEventListener('click', ev => {

        //right image click
        let lastIcon = +ev.target.dataset.id;

        let dir = ev.target.dataset.direction;
        let target = +ev.target.dataset.id;

        if (validate(lastIcon, target, dir)) {
            changeIconsBG(lastIcon, dir);
            addActiveClass(dir);
            changeMainBG(target, dir);
        }
    });

    navBar[5].addEventListener('click', ev => rightArrowListener(ev));


    function startScreen() {

        let randClick = Math.floor(Math.random() * 6 + 10);

        for (let i = 0; i < randClick; i++) {
            let randImgNav = Math.floor(Math.random() * 4 + 1);
            navBar[randImgNav].click();
        }
    }
    startScreen();


})
