'use strict'

window.addEventListener('load', () => {

    let navBar = document.querySelectorAll('.work-tabs-title');
    let loadBtn = document.querySelector('#work .btn');
    loadBtn.style.display = 'none';
    let imgContainer = document.querySelector('.work-images');
    let content = [];
    let count = 0;

    navBar.forEach( el => {

        el.dataset.category = `category${count}`;

        el.addEventListener('click', () => {
            navBar.forEach(el => el.classList.remove('active'));
            el.classList.add('active');
            content = [];

            if (el.dataset.category === 'category0') {
                for (let cat in portfolio) {
                    content.concat(getContent(portfolio[cat]));
                }
                arrShuffle(content);
                content.length > 12 ? loadBtn.style.display = 'block' : loadBtn.style.display = 'none';
            } else {
                content = getContent(portfolio[el.dataset.category]);
                arrShuffle(content);
                content.length > 12 ? loadBtn.style.display = 'block' : loadBtn.style.display = 'none';
            }

            imgContainer.innerHTML = '';
            let first12 = content.slice(0, 12);

            while (first12.length) {
                imgContainer.insertAdjacentHTML('beforeend', content.shift());
                first12.shift();
            }
        });

        count++;
    });

    loadBtn.addEventListener('click', () => {

        let duration = 2000;
        loaderEmulator(loadBtn, duration);

        setTimeout(() => {
            for (let i = 0; i < 12; i++) {
                content.length ? imgContainer.insertAdjacentHTML('beforeend', content.shift()) : false;
            }
            content.length > 0 ? loadBtn.style.display = 'block' : loadBtn.style.display = 'none';

        }, duration)

    });

    function getContent(...objects) {
        for (let i = 0; i < objects.length; i++) {
            objects[i].images.map(el => {
                content.push(`
                                <div class="work-images-wrapper">
                                    <img src="img/${objects[i].path}/${el}.jpg" alt="${objects[i].path} image ${el}" class="work-images-item">
                                    <div class="work-images-flip-card">
                                        <img src="img/work-flip-card-img.png" alt="flip card icon">
                                        <p class="fc-turq">creative design</p>
                                        <p>${objects[i].path}</p>
                                    </div>
                                </div>
                            `);
            }).filter(Boolean).join('');
        }
        return content;
    }

    function arrShuffle(array) {
        let currentIndex = array.length;
        let randomIndex;

        while (currentIndex !== 0) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;
            [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
        }
        return array;
    }

    document.querySelector('.work-tabs-title').click();
})