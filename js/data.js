'use strict'

const portfolio = {
    //do not use 'category0' name! its always 'all' category
    //order in slave objects required! next elem: 'category += 1'
    //path in category should repeat photo directory name from root/img/
    //images array should contain photo name (without extension), and should be .jpg!

    category1 : {
        path : 'graphic-design',
        images : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, /*14,*/ '15---',],
    },
    category2 : {
        path : 'web-design',
        images : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,],
    },
    category3 : {
        path : 'landing-page',
        images : [1, 2, 3, 4, 5, 6, 7, 8, 9,],
    },
    category4 : {
        path : 'wordpress',
        images : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,],
    },
};

const slider = {
    //order in slave objects required! next elem: property += 1: id +=1
    //imgLink should repeat path to photo from root of the project

    1 : {
        id : 1,
        header: 'Nataly',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Junior SE',
        imgLink: 'img/slider/1.png',
    },
    2 : {
        id : 2,
        header: 'Anabel',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Middle SE',
        imgLink: 'img/slider/2.png',
    },
    3 : {
        id : 3,
        header: 'Margo',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Senior SE',
        imgLink: 'img/slider/3.png',
    },
    4 : {
        id : 4,
        header: 'Susanna',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Team lead',
        imgLink: 'img/slider/4.png',
    },
    5 : {
        id : 5,
        header: 'Tracy',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Middle SE',
        imgLink: 'img/slider/5.png',
    },
    6 : {
        id : 6,
        header: 'Kristina',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Junior SE',
        imgLink: 'img/slider/6.png',
    },
    7 : {
        id : 7,
        header: 'Olimpia',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Middle SE',
        imgLink: 'img/slider/7.png',
    },
    8 : {
        id : 8,
        header: 'Jessika',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Junior SE',
        imgLink: 'img/slider/8.png',
    },
    9 : {
        id : 9,
        header: 'Vanessa',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Designer',
        imgLink: 'img/slider/9.png',
    },
    10 : {
        id : 10,
        header: 'Anna',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Designer',
        imgLink: 'img/slider/10.png',
    },
    11 : {
        id : 11,
        header: 'Valery',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Middle QA engineer',
        imgLink: 'img/slider/11.png',
    },
    12 : {
        id : 12,
        header: 'Marina',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Project manager',
        imgLink: 'img/slider/12.png',
    },
    13 : {
        id : 13,
        header: 'Linda',
        text1: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.\n' +
            '   Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '   dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        text2: 'Junior QA engineer',
        imgLink: 'img/slider/13.png',
    },
};
